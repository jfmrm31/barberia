<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;
$tdl_options = woodstock_global_var();

$upsells = $product->get_upsells();

if ( sizeof( $upsells ) == 0 ) {
	return;
}

$meta_query = WC()->query->get_meta_query();

$args = array(
	'post_type'           => 'product',
	'ignore_sticky_posts' => 1,
	'no_found_rows'       => 1,
	'posts_per_page'      => $posts_per_page,
	'orderby'             => $orderby,
	'post__in'            => $upsells,
	'post__not_in'        => array( $product->id ),
	'meta_query'          => $meta_query
);

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;

if ( $products->have_posts() ) : ?>

    <?php 

        if ( ( !isset($tdl_options['tdl_related_products_per_view']) ) ) {
            $products_per_column = 4;
        } else {
            $products_per_column = $tdl_options['tdl_related_products_per_view'];
        }


    if ($products_per_column == 6) {
        $products_per_column_xlarge = 6;
        $products_per_column_large = 4;
        $products_per_column_medium = 3;
    }

    if ($products_per_column == 5) {
        $products_per_column_xlarge = 5;
        $products_per_column_large = 4;
        $products_per_column_medium = 3;
    }

    if ($products_per_column == 4) {
        $products_per_column_xlarge = 4;
        $products_per_column_large = 4;
        $products_per_column_medium = 3;
    }

    if ($products_per_column == 3) {
        $products_per_column_xlarge = 3;
        $products_per_column_large = 3;
        $products_per_column_medium = 2;
    }

    if ($products_per_column == 2) {
        $products_per_column_xlarge = 2;
        $products_per_column_large = 2;
        $products_per_column_medium = 2;
    }
    ?>

	<div class="upsells products">

		<h2><?php esc_html_e( 'You may also like&hellip;', 'woocommerce' ) ?></h2>

        <ul id="products" class="products products-grid small-block-grid-2 medium-block-grid-<?php echo esc_attr($products_per_column_medium); ?> large-block-grid-<?php echo esc_attr($products_per_column_large); ?> xlarge-block-grid-<?php echo esc_attr($products_per_column_xlarge); ?> xxlarge-block-grid-<?php echo esc_attr($products_per_column); ?> columns-<?php echo esc_attr($products_per_column); ?> product-layout-grid">

			<?php while ( $products->have_posts() ) : $products->the_post(); ?>

				<?php wc_get_template_part( 'content', 'product' ); ?>

			<?php endwhile; // end of the loop. ?>

		</ul>

	</div>

<?php endif;

wp_reset_postdata();
