<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'barberia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'TxPEb6/U22k3]GK~hZ6)lo3G~/&SsF[vaKc+(~2:%,S.rX?8vbQoF@-<Fo_u#G$7');
define('SECURE_AUTH_KEY',  '/3~3{(2f.f_@|noId[m83e[abD>L*FKi:ZOKtA[5`[|f^CsNXq5[PR,{/||/kUOl');
define('LOGGED_IN_KEY',    'N.hHfrV>?]/2NWlC?n!%AqGaZLAjYka!*XgNHnO*YBq_ABtqz0=Yo]y`Y-,W5oci');
define('NONCE_KEY',        '#7u;{C(S{.G1uJkHLgH}b MIamwl#ha#C;QHv5A>(Mpt8]o/#~:qjSjdvBa!3uVI');
define('AUTH_SALT',        'F=>-=8Y]~J6paDnB[={L1SU&D.eT#@a?_]~%);Jke*Go73Bz6uP2]h8Rs@Xdz1Oe');
define('SECURE_AUTH_SALT', 'oUWfSyR`nUbZd6h+N4_&kLjo,VnRbMkGgy.TEy5xxg0KqBdJ%^;xKF8 !~{[M._O');
define('LOGGED_IN_SALT',   '!7w6cX.(3xqzBKV2hNmZ6uSq3Q~6>/Tf(S^-#X5-qT|JTI/Ha.ddP.2qJt@3;[4d');
define('NONCE_SALT',       '+r%N&(]he}:iny8`0}0;(SP(P,oFwmrF_/F{6Sbvja4*;T)-jl4&h)_=)c&Z+f_k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
